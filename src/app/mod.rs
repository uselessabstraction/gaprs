//  gAPRS - A GTK+ APRS application
//  Copyright (C) 2019  Albert James Brown <uselessabstraction@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod resources;
pub mod state;
pub mod window;

pub use self::state::*;
pub use self::window::*;

use std::sync::{Arc, Mutex};

use gio::{ApplicationExt, ApplicationExtManual};
use gtk::prelude::*;

use crate::constants::*;

pub struct App {
    builder: gtk::Builder,
    state: Arc<Mutex<State>>,
    window: window::Window
}

impl App {
    pub fn run() -> i32 {
        let gtk_app = gtk::Application::new(APP_ID, gio::ApplicationFlags::empty())
            .expect("Failed to create GTK+ application!");

        gtk_app.set_property_resource_base_path(Some("/com/gitlab/uselessabstraction/gaprs"));

        // Startup
        gtk_app.connect_startup(move |gtk_app| {
            let builder = gtk::Builder::new();
            builder.add_from_resource("/com/gitlab/uselessabstraction/gaprs/ui/app_window.ui")
                .expect("Couldn't find app_window in GResource!");

            let gtk_window: gtk::ApplicationWindow = builder.get_object("app_window")
                .expect("Couldn't find 'app_window' in UI");
            gtk_window.set_application(gtk_app);

            let state = Arc::new(Mutex::new(State::new()));

            let window = Window::new();
            
            let app = App {
                builder: builder,
                state: state,
                window: window
            };

            app.init()

        });

        // Activate
        gtk_app.connect_activate(move |_| {
            println!("Activate!");
        });
        
        return gtk_app.run(&[]);
    }

    fn init (&self) {
        self.window.build(self.builder.clone());
        self.window.show();
    }
}
