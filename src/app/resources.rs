//  gAPRS - A GTK+ APRS application
//  Copyright (C) 2019  Albert James Brown <uselessabstraction@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

extern crate gio;
extern crate glib;

use gio::{resources_register, Resource};
use glib::Bytes;

pub fn init() {
    // Include compiled GResource in binary, register it with GIO
    let res_inc = include_bytes!("../../res/resources.gresource");
    let gbytes = Bytes::from(res_inc.as_ref());
    let gresource = Resource::new_from_data(&gbytes)
        .expect("Error parsing compiled GResource");
    resources_register(&gresource);
}
