//  gAPRS - A GTK+ APRS application
//  Copyright (C)  2019 Albert James Brown <uselessabstraction@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

//use gio::{SimpleActionExt, ActionMapExt, MenuExt, ActionExt};
use gtk::prelude::*;

use util;

//#[derive(Clone)]
pub struct Window {
    window:          Option<gtk::ApplicationWindow>,
    sidebar:         Option<gtk::Stack>,
    main_view:       Option<gtk::Stack>,
    map_button:      Option<gtk::Button>,
    messages_button: Option<gtk::Button>,
    packets_button:  Option<gtk::Button>,
    settings_button: Option<gtk::Button>,
}

impl Window {
    pub fn new() -> Self {
        Window {
            window:          None,
            sidebar:         None,
            main_view:       None,
            map_button:      None,
            messages_button: None,
            packets_button:  None,
            settings_button: None
        }
    }
    
    pub fn build(&self, builder: gtk::Builder) { 
//        let window = self.window;

        // Grab window
        self.window = builder.get_object("app_window");
    }
/*
        // Grab widgets from builder
        let sidebar: gtk::Stack = builder.get_object("sidebar_stack")
            .expect("Couldn't locate 'sidebar_stack' in resources");
        let main_view: gtk::Stack = builder.get_object("view_stack")
            .expect("Couldn't locate 'view_stack' in resources");
        let map_button: gtk::Button = builder.get_object("view_map_button")
            .expect("Couldn't locate 'view_map_button' in resources");
        let messages_button: gtk::Button = builder.get_object("view_messages_button")
            .expect("Couldn't locate 'view_messages' in resources");
        let packets_button: gtk::Button = builder.get_object("view_packets_button")
            .expect("Couldn't locate 'view_packets_button' in resources");
        let settings_button: gtk::Button = builder.get_object("view_settings_button")
            .expect("Couldn't locate 'view_settings_button' in resources");

        // Connect signal handlers
        map_button.connect_clicked(clone!(main_view => move |_| {
            main_view.set_visible_child_name("map_view");
        }));
        messages_button.connect_clicked(clone!(main_view => move |_| {
            main_view.set_visible_child_name("message_view");
        }));
        packets_button.connect_clicked(clone!(main_view => move |_| {
            main_view.set_visible_child_name("packet_view");
        }));
        settings_button.connect_clicked(clone!(main_view => move |_| {
            main_view.set_visible_child_name("settings_view");
        }));
         */
    pub fn show(&self) {
        if let Some(ref w) = self.window {
            w.show();
        }
    }
}
