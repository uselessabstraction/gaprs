//  gAPRS - A GTK+ APRS application
//  Copyright (C)  2019 Albert James Brown <uselessabstraction@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

extern crate ax25;
extern crate gio;
extern crate glib;
extern crate gtk;

use std::net::TcpStream;
use ax25::frame::*;
mod aprs;

mod constants;
#[macro_use]
mod util;

//mod app;

//use constants::*;

fn main() {
    let stream = TcpStream::connect("localhost:8001").expect("Connection failed.");
    let kiss = aprs::kiss::Decoder::new(stream);
    for frame in kiss {
        println!("Recieved KISS frame:");
        match frame.command {
            aprs::kiss::Command::Data(d) => {
                match Ax25Frame::from_bytes(d.as_slice()) {
                    Ok(frame) => {
                        println!("  AX.25 Frame:");
                        println!("    Source: {}", frame.source);
                        println!("    Destination: {}", frame.destination);
                        print!("    Route: ");
                        for entry in &frame.route {
                            print!("{} ", entry.repeater);
                        }
                        print!("\n");
                     if let Some(info) = frame.info_string_lossy() {
                         println!("    Info: {}", info);
                     }
                        print!("\n");
                    }
                    Err(_) => {
                        println!("Ax25Frame::from_bytes(...) failed!\n");
                    }
                }
            }
            _ => {
                println!("Unexpected KISS command!");
            }
        }
    }
    
    /*let exit_code;

    {
        println!("{} {}", APP_NAME, APP_VERSION);
        app::resources::init();
        
        exit_code = app::App::run();
    }
    
    std::process::exit(exit_code);*/
}

// fn kiss_test() {
//     let addr = "127.0.0.1:8001";

//     println!("Connecting to {} ...", addr);
//     let mut kiss = KissTcp::connect(addr).expect("Connection Failed!");
//     println!("Connection successful!");
//     loop {
//         if let Ok(buffer) = kiss.recv_frame_bytes() {
//             println!("Recieved Frame. Length: {}", buffer.len());
//             //println!("Contents: {:x?}\n", buffer);
//             match Ax25Frame::from_bytes(buffer.as_slice()) {
//                 Ok(frame) => {
//                     println!("Source: {}", frame.source);
//                     println!("Destination: {}", frame.destination);
//                     print!("Route: ");
//                     for entry in &frame.route {
//                         print!("{} ", entry.repeater);
//                     }
//                     print!("\n");
//                     if let Some(info) = frame.info_string_lossy() {
//                         println!("Info: {}", info);
//                     }
//                     print!("\n");
//                 }
//                 Err(_) => {
//                     println!("Ax25Frame::from_bytes(...) failed!\n");
//                 }
//             }
//         } else {
//             break;
//         }
//     }
//     println!("Done.");
// }
