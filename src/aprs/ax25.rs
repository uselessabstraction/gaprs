//  gAPRS - A GTK+ APRS application
//  Copyright (C) 2019  Albert James Brown <uselessabstraction@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

//
// AX.25 frame construction:
//  _________________________________________________________________
// | Flag |    Address     |   Contro  l  |  Info   |   FCS   | Flag |
// | 0x7E | 14 or 28 bytes | 1 or 2 bytes | N Bytes | 2 bytes | 0x7e |
// (Unnumbered and Supervisory frames)
//  __________________________________________________________________________
// | Flag |    Address     |   Control    |   PID  |  Info   |   FCS   | Flag |
// | 0x7E | 14 or 28 bytes | 1 or 2 bytes | 1 byte | N Bytes | 2 bytes | 0x7e |
// (Information frames)
//
// APRS only uses a small subset of the AX.25 protocol, so we probably won't
// rush to implement every last fine detail of the spec.  Maybe we can do it
// one day if we decide to create a new AX.25 packet network with blackjack and
// hookers.
//

pub struct Address {
    callsign: String,
    ssid: u8
}

pub struct Frame {
    source: Address,
    destination: Address,
    info: Box<[u8]>
}

