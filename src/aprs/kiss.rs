//  gAPRS - A GTK+ APRS application
//  Copyright (C) 2019  Albert James Brown <uselessabstraction@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// See http://www.ka9q.net/papers/kiss.html for technical details

use std::slice;
use std::error::Error;
use std::fmt;
use std::io::{Read};
use std::result::Result;

// The KISS Protocol uses a couple magic numbers to identify frames.  The
// beginning and end of a frame is marked with FEND (C0).  FEND can be escaped
// with a FESC TFEND (DB DC) sequence.  Likewise, FESC can be escaped with a
// FESC TFESC sequence (DB DD).  If an FESC is followed by anything other than
// TFEND or TFESC, it is technically an error - however - the escape should be
// cancelled and processing should continue gracefully.

static FEND: u8  = 0xC0;        // Frame End - Marks beginning and end of frame 
static FESC: u8  = 0xDB;        // Frame Escape - Puts reader in 'escape mode'
static TFEND: u8 = 0xDC;        // Insert FEND if in escape mode
static TFESC: u8 = 0xDD;        // Insert FESC if in escape mode

// The first byte of a KISS frame includes a four bit command code.  These can
// be used by the computer to set a handful of TNC parameters.  There is no
// success/failure response from the TNC.  The TNC will only send Data frames.
//
// All time parameters are supplied in 10ms units (e.g. 50 = 500ms).
#[derive(Debug)]
pub enum Command {
    Data(Vec<u8>),              // Data frame to send/recive over the interface
    TxDelay(u8),                // Delay between PTT activation and transmission
    Persistence(u8),            // Controls exponential back-off
    SlotTime(u8),               // Delay between attempts
    TxTail(u8),                 // Obsolete
    FullDuplex(bool),           // Enable duplex for hardware which supports it
    SetHardware(Vec<u8>),       // Implementation is TNC-specific
    Unknown
}

#[derive(Debug)]
pub struct Frame {
    pub channel: u8,
    pub command: Command
}

impl Frame {
    pub fn from_bytes(bytes: &[u8]) -> Result<Frame, Box<Error>> {
        if bytes.len() < 2 {
            return Err(Box::new(ParseError::with_msg("KISS Frame is too short.")));
        }
        let channel = bytes[0] & 0b1111;
        let command_id = bytes[0] >> 4;
        let command = match command_id {
            0 => Command::Data(bytes[1..].to_vec()),
            1 => Command::TxDelay(bytes[2]),
            2 => Command::Persistence(bytes[2]),
            3 => Command::SlotTime(bytes[2]),
            4 => Command::TxTail(bytes[2]),
            5 => Command::FullDuplex(match bytes[2] {0 => false, _ => true}),
            6 => Command::SetHardware(bytes[1..].to_vec()),
            _ => Command::Unknown
        };
            
        Ok(Frame {
            channel: channel,
            command: command
        })
    }
}

pub struct Decoder<R: Read>
{
    reader: R,
    error: Option<Box<Error>>
}

impl<R: Read> Decoder<R> {
    pub fn new(reader: R) -> Self {
        Self {
            reader: reader,
            error: None
        }
    }
    
    pub fn read_frame(&mut self) -> Result<Frame, Box<Error>> {
        let mut buffer: Vec<u8> = Vec::with_capacity(512);
        let mut escape = false;
        loop {
            let mut byte = 0u8;
            self.reader.read(slice::from_mut(&mut byte))?;

            if byte == FEND {
                if !buffer.is_empty() {
                    return Ok(Frame::from_bytes(buffer.as_slice())?);
                } else {
                    continue;
                }
            } else if byte == FESC {
                escape = true;
                continue;
            } else if escape {
                if byte == TFEND {
                    buffer.push(FEND);
                } else if byte == TFESC {
                    buffer.push(FESC);
                } else {
                    // This should fail gracefully. but for now I want to see
                    // if it happens
                    println!("Invalid Escape sequence in KISS frame!");
                    buffer.push(byte);                    
                }
            }

            buffer.push(byte);
            escape = false;
        }
    }
}

impl<R: Read> Iterator for Decoder<R> {
    type Item = Frame;
    
    fn next(&mut self) -> Option<Self::Item> {
        match self.read_frame() {
            Ok(frame) => Some(frame),
            Err(e) => {
                self.error = Some(e);
                None
            }
        }
    }
}

#[derive(Debug)]
pub struct ParseError {
    msg: String
}

impl ParseError {
    fn new() -> Self {
        ParseError {
            msg: "KISS Parse Error".to_string()
        }
    }

    fn with_msg(msg: &str) -> Self {
        ParseError {
            msg: msg.to_string()
        }
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl Error for ParseError {
    fn description(&self) -> &str {
        return &self.msg;
    }
}
